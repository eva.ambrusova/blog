class FixDefaultValues < ActiveRecord::Migration[5.1]
  def change
    change_column :comments, :likes, :integer, :default => 0
    change_column :comments, :dislikes, :integer, :default => 0
    change_column :post_options, :blocked, :boolean, :default => false
  end
end
