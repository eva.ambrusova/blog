class RemoveDislikesFromComment < ActiveRecord::Migration[5.1]
  def change
    remove_column :comments, :dislikes, :integer
  end
end
