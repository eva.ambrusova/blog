class RemoveLikesFromComments < ActiveRecord::Migration[5.1]
  def change
    remove_column :comments, :likes, :integer
  end
end
