class RemoveColumnsFromPosts < ActiveRecord::Migration[5.1]
  def change
    remove_column :posts, :comment_allowed
    remove_column :posts, :published
  end
end
