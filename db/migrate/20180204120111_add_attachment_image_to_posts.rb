class AddAttachmentImageToPosts < ActiveRecord::Migration[5.0]
  def self.up
    change_table :posts do |t|
       add_attachment :posts, :image
    end
  end

  def self.down
    remove_attachment :posts, :image
  end
end
