class CreatePostOptions < ActiveRecord::Migration[5.1]
  def change
    create_table :post_options do |t|
      t.belongs_to :post, index: true
      t.boolean :public
      t.boolean :comments_allowed
      t.boolean :blocked
    end
  end
end
