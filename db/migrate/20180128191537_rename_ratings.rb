class RenameRatings < ActiveRecord::Migration[5.1]
  def change
    rename_column :ratings, :rating, :value

  end
end
