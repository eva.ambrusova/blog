class AddCommentNeedsApprovalToPostOptions < ActiveRecord::Migration[5.1]
  def change
    add_column :post_options, :comment_needs_approval, :boolean
  end
end
