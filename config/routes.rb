Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: {registrations: "registrations"}
  post 'likes/create'
  post 'likes/destroy'
  post 'dislikes/create'
  post 'dislikes/destroy'
  post 'comments/reply'
  post 'comments/approve'
  get 'post/my', to: 'posts#my', as: :my_posts
  get 'posts/index', to: 'posts#index', as: :post_index

  resources :posts do
    resources :comments
    resources :ratings
  end

  resources :admin do
    resources :tags

    root to: "tags#index"
  end

  resources :tags
  resources :users

  root 'posts#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
