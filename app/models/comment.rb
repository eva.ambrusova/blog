class Comment < ApplicationRecord
  acts_as_tree order: 'created_at DESC'
  belongs_to :user
  belongs_to :post #, dependent: :destroy
  has_many :like, dependent: :destroy
  has_many :dislike, dependent: :destroy

  validates :title, presence: {message: "Missing title"}
  validates :content, presence: {message: "Missing content"}

  before_save :generate_timestamp

  def generate_timestamp
    self.added = DateTime.now
  end

  def likes_count
    self.like.count()
  end

  def can_like id
    self.like.where(user_id: id).empty?
  end

  def dislikes_count
    self.dislike.count()
  end

  def can_dislike id
    self.dislike.where(user_id: id).empty?
  end

end
