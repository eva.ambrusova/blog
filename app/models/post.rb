class Post < ApplicationRecord
  has_attached_file :image, styles: { large: "900x900>", medium: "500x500>", thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/


  has_many :comments, dependent: :destroy

  has_many :ratings, dependent: :destroy
  has_and_belongs_to_many :tags

  belongs_to :user

  has_one :post_option

  validates :title, presence: {message: "Missing title"}
  validates :perex, presence: {message: "Missing perex"}
  validates :text, presence: {message: "Missing text"}

  before_save :generate_timestamp
  before_create :build_associations

  def generate_timestamp
    self.added = DateTime.now
  end

  def assign_params_from_controller(params)
    @option_params = params
  end

  def build_associations
    #PostOption.new(@option_params).save
    self.post_option = PostOption.new(@option_params)
  end

  def primary_comments
    self.comments.where(parent_id: nil, approved: true).order(created_at: :desc)
  end

  def not_allowed_comments_for_current_user id
    self.comments.where(user_id: id, approved: false).order(created_at: :desc)
  end

  def not_allowed_comments
    self.comments.where(approved: false).order(created_at: :desc)
  end

end
