class Rating < ApplicationRecord
  belongs_to :user
  belongs_to :post
  validates :value, numericality: { only_integer: true }, inclusion: { :in => [ 1, 2, 3, 4, 5 ] }
end
