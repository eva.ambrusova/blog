class UnderComment < ApplicationRecord
  belongs_to :comment, dependent: :destroy
end
