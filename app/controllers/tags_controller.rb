class TagsController < ApplicationController
  before_action :authenticate_user!

  def index
      @tags = Tag.all

  end

  ### show detail of tag
  def show
      @tag = Tag.find(params[:id])
  end

  ### new tag
  def new
    if current_user.admin?
      @tag = Tag.new
    else
       redirect_to root_path
    end
  end

  ### edit tag
  def edit
    if current_user.admin?
      @tag = Tag.find(params[:id])
    else
       redirect_to root_path
    end
  end

  ### create new tag
  def create
    if current_user.admin?
      @tag = Tag.new(tag_params)

      if @tag.save
        redirect_to @tag
      else
        #redirect back
        redirect_back fallback_location: root_path

        #show error message
        @tag.errors.messages.each do |msg|
          flash[:warning] = msg.second
        end
      end
    else
       redirect_to root_path
    end
  end

  ### update tag
  def update
    if current_user.admin?
      @tag = Tag.find(params[:id])

      if @tag.update(tag_params)
        redirect_to @tag
      else
        render 'edit'
      end
    else
       redirect_to root_path
    end
  end

  ### destroy chosen tag
  def destroy
    if current_user.admin?
      @tag = Tag.find(params[:id])
      @tag.destroy

      redirect_to tags_path
    else
       redirect_to root_path
    end
  end

  ### tag parameters
  private
  def tag_params
    params.require(:tag).permit(:name)
  end
end
