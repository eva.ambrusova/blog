class DislikesController < ApplicationController

  def new
    @dislike = Dislike.new(like_params)
  end

  def create

    @post = Post.find(params[:post_id])

    #destroy like if exists
    @likes = Like.where(user_id: params[:user_id],comment_id: params[:comment_id])
    unless @likes.empty?
      @likes.first.destroy
    end

    @dislike = Dislike.new(dislike_params)
    @comment = Comment.find(params[:comment_id])
    @dislike.save

    respond_to do |format|
      format.js
    end
  end

  def destroy

    @post = Post.find(params[:post_id])
    @dislike = Dislike.where(user_id: params[:user_id],comment_id: params[:comment_id]).take
    @dislike.destroy

    respond_to do |format|
      format.js
    end

  end

  private
  def dislike_params
    params.permit(:user_id, :comment_id)
  end

end
