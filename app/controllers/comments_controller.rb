class CommentsController < ApplicationController
  def index
    @comments = Comment.all
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def new
    @comment = Comment.new(parent_id: params[:parent_id])
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def create
    @comment = Comment.new(comment_params)

    @comment.approved = !PostOption.where(post_id: params[:post_id]).take.comment_needs_approval

    if @comment.save
      redirect_to post_path(Post.find(params[:post_id]))
    else
      #redirect back
      redirect_back fallback_location: root_path

      #show error message
      @comment.errors.messages.each do |msg|
        flash[:warning] = msg.second
      end

    end
  end

  def reply


    if params[:comment][:parent_id].to_i > 0

      # metoda comment_params nepovoluje parent_id, pretoze ked vytvaram comment bez parenta, nechcem tam mat tu polozku
      # preto treba delete(:parent_id)
      parent_comment = Comment.find_by_id(params[:comment].delete(:parent_id))

      # vytvorenie noveho commentu
      @comment = Comment.new(comment_params)

      #add_child vytvori vazby v stromovej strukture (ancestor, descendant, generation)
      parent_comment.add_child(@comment)


      @comment.save

      redirect_to post_path(Post.find(parent_comment.post_id))
    end
  end

  def update
    @comment = Comment.find(params[:id])

    if @comment.update(comment_params)
      redirect_to @comment
    else
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    redirect_to post_path(@post)
  end

  def approve
    @comment = Comment.find(params[:id])
    @comment.approved = true
    @comment.save

    redirect_back fallback_location: root_path


  end

  private
  def comment_params
    params.require(:comment).permit(:title, :content, :added, :likes, :dislikes, :approved, :user_id, :post_id)
  end

end
