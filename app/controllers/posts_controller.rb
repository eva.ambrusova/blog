class PostsController < ApplicationController
  # before_action :authenticate_user!

  def index

    if params[:tag]
      #filtered public posts
      @posts = public_posts.joins(:tags).where('name LIKE ?', "%#{params[:tag]}%")
    else
      #all public posts
      @posts = public_posts
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find(params[:id])
  end

  def create
    @post = Post.new(post_params)
    @post.assign_params_from_controller(post_option_params)

    if @post.save
      redirect_to @post
    else
      #redirect back
      redirect_back fallback_location: root_path

      #show error message
      @post.errors.messages.each do |msg|
        flash[:warning] = msg.second
      end
    end
  end

  def update
    @post = Post.find(params[:id])

    if @post.update(post_params) and @post.post_option.update(post_option_params)
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    redirect_to posts_path
  end

  def my
    @current_user_posts = Post.joins(:user).where('users.id = ?', current_user.id)
  end

  private
  def post_params
    params.require(:post).permit(:title, :perex, :text, :added, :image, :user_id, :tag_ids => [])
  end

  def post_option_params
    params.require(:post_options).permit(:public, :comments_allowed, :comment_needs_approval)
  end

  def public_posts
    Post.joins(:post_option).where('post_options.public = true')
  end


end
