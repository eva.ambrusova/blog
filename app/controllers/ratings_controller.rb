class RatingsController < ApplicationController
  

  def index
    @ratings = Rating.all
  end

  def show
    @rating = Rating.find(params[:id])
  end

  def new
    @rating = Rating.new
  end

  def edit
    @rating = Rating.find(params[:id])
  end

  def create

    #check if current user already rated this post
    ratings = Rating.where(user_id: current_user.id, post_id: params[:post_id])
    if ratings.empty?
      @rating = Rating.new(rating_params)
      @rating.save
      redirect_back(fallback_location: root_path)
    else
      params[:rating][:id] = ratings.first.id
      self.update
    end

  end

  def update
    @rating = Rating.find(params[:rating][:id])

    @rating.update(rating_params)
    redirect_back(fallback_location: root_path)
  end

  def destroy
    @post = Post.find(params[:post_id])
    @rating = @post.ratings.find(params[:id])
    @rating.destroy
    redirect_to post_path(@post)
  end

  private
  def rating_params
    params.require(:rating).permit(:value, :user_id, :post_id)
  end
end
