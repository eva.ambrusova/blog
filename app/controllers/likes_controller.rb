class LikesController < ApplicationController

  def new
    @like = Like.new(like_params)
  end

  def create

    @post = Post.find(params[:post_id])

    #destroy dislike if exists
    @dislikes = Dislike.where(user_id: params[:user_id],comment_id: params[:comment_id])
    unless @dislikes.empty?
      @dislikes.first.destroy
    end

    @like = Like.new(like_params)
    @comment = Comment.find(params[:comment_id])
    @like.save

    respond_to do |format|
      format.js
    end
  end

  def destroy

    @post = Post.find(params[:post_id])
    @like = Like.where(user_id: params[:user_id],comment_id: params[:comment_id]).take
    @like.destroy

    respond_to do |format|
      format.js
    end

  end

  private
  def like_params
    params.permit(:user_id, :comment_id)
  end

end
